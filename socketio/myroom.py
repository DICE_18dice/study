#!/usr/bin/env python3.7
from socketIO_client_nexus import SocketIO, BaseNamespace
from base64 import b64encode
import os.path
#import asyncio
#loop = asyncio.get_event_loop()
#each node in MS has an unique mac (ie. ID)
if os.path.isfile("./.mac"):
    fo = open("./.mac", "r")
    mac=fo.read()
    fo.close()
else:
    mac=''
mac64=b64encode(bytes('{'+mac+'}', 'utf-8'))


def on_get_settings(*args):
    print('on_get_settings',args)

def on_plugin(*args):
    print('on_plugin',args[0]['data']['mac'])
    mac=args[0]['data']['mac']
    fo = open(".mac", "w")
    fo.write( mac )
    fo.close()

def on_join(*args):
    print('on_join',args)

def on_broadcast_to_room(*args):
    print('on_broadcast_to_room',args)


class MSNamespace(BaseNamespace):

    def on_connect(self):
        print('[Connected]')

    def on_reconnect(self):
        print('[Reconnected]')

    def on_disconnect(self):
        print('[Disconnected]')
        start_server()

    def on_welcome(*args):
        print('on_welcome_response', args)

    def on_ps_message(*args):
        print('on_ps_message', args)

    def on_broadcast_room(*args):
        print('on_broadcast_room', args)

def start_server():
   msns = MSNamespace
   socketIO = SocketIO('office.18dice.tw', 6070, msns,params={'info': mac64})
   mss = socketIO.define(MSNamespace, '/MessageServer')
   # Listen
   mss.on('disconnect', MSNamespace.on_disconnect)
   mss.on('reconnect', MSNamespace.on_reconnect)
   mss.on('misc#welcome', MSNamespace.on_welcome)
   mss.on('ps_message', MSNamespace.on_ps_message)          #from client
   mss.on('broadcast_room', MSNamespace.on_broadcast_room)  #from dice server

   mss.on('connect', MSNamespace.on_connect)
   #mss.emit('get_settings',on_get_settings)
   mss.emit('plugin',{'mac':mac},on_plugin)
   socketIO.wait_for_callbacks(seconds=1)
   mss.emit('join','jjRoom1',on_join)
   mss.emit('broadcast_room','jjRoom1',{'xx':123,'oo':'abc'},on_broadcast_to_room)

   socketIO.wait_for_callbacks(seconds=1)
   socketIO.wait()

if __name__ == '__main__':
    #loop.run_until_complete(start_server())
    start_server()
