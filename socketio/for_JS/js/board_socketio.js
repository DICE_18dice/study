var _login_users={},_save_login,_blocking=false;
var _user_info={};
var _bk_r=0,_bk_c=0;
var _exercise_keys=[];
function handle_socketio(ds_socket){
	    /*
		$( "div" ).mousemove(function( event ) {
		  var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
		  var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";

		});
		*/
    	// socket.io specific code
		ds_socket.on('connect', function () {
		  $.unblockUI();
		  showState('rgba(255, 255, 0, 0.3)','fa fa-eye fa-3x','MS主機已連線, 等候Dice Server');
	  	  plugin();
	  	  _is_connected=true;
	     });
		ds_socket.on('misc#welcome', function (data) {
			 _is_connected=true;
		 });

		ds_socket.on('error', function (err) {
			 _is_connected=false;
			 if (! _blocking){
				 $.blockUI({ message: 'MS主機斷線!' });
				 _blocking=true;
			 }
		 });
		ds_socket.on('disconnect', function (err) {
			showState('rgba(255, 0, 0, 1)','fa fa-eye-slash fa-3x','MS主機斷線');
			_is_connected=false;
			if (! _blocking){
				 $.blockUI({ message: 'MS主機斷線' });
				 _blocking=true;
			 }
		 });
		ds_socket.on('reconnect', function (times) {
			showState('rgba(255, 255, 0, 1)','fa fa-eye fa-3x','重新連線MS主機');
		  	 _is_connected=true;
		  	 $.unblockUI();
			_blocking=false;
		 });
		ds_socket.on('reconnecting', function (times) {
			showState('rgba(255, 255, 0, 1)','fa fa-eye fa-3x','正嘗試重新第'+times+'連線MS主機中');
			_is_connected=false;
			if (_blocking){
				$.unblockUI();
				_blocking=false;
			}
			$.blockUI({ message: '正嘗試重新第'+times+'連線MS主機中' });
			_blocking=true;
			
		});
		ds_socket.on('reconnect_failed', function () {
			showState('rgba(255, 0, 0, 1)','fa fa-chain-broken fa-3x','MS主機重新連線失敗');
		    _is_connected=false;
		    if (! _blocking){
				 $.blockUI({ message: 'MS主機重新連線失敗, 請手動重載' });
				 _blocking=true;
			 }
		});
		ds_socket.on('reconnect_error', function (err) {
			showState('rgba(255, 0, 0, 1)','fa fa-chain-broken fa-3x','MS主機連線錯誤'+err);
		  	 _is_connected=false;
		  	if (! _blocking){
				 $.blockUI({ message: 'MS主機連線錯誤, 請手動重載' });
				 _blocking=true;
			 }
		});
		ds_socket.on('ports_changed', function (result) {
		} );
		ds_socket.on('message', function (result) {
			exec(result);
		} );
		ds_socket.on('joined', function (result) {
			//jj_notify("info","有人加入教室: "+JSON.stringify(result)+"");
		});
		ds_socket.on('left', function (result) {
			//jj_notify("info","有人離開教室: "+JSON.stringify(result)+"")
		} );
		ds_socket.on('watch_it', function (result) {
			//console.log("watch_it: "+JSON.stringify(result)+"");
			if (result.mac.mac==_choiced_mac){
				 jj_notify("error","Dice Server "+_choiced_mac+" 中斷..請稍後重新載入.");
				 showState('rgba(255, 0, 0, 1)','fa fa-chain-broken fa-3x',"Dice Server "+_choiced_mac+" 中斷..請稍後重新載入.");
				 $('#loginnow .k12').css('cursor','default');
				 $('#loginnow .k12').text("Dice Server "+_choiced_mac+" 中斷..請稍後重新載入.");
				 if (! _blocking){
	 				 $.blockUI({ message: 'DICE Server'+_choiced_mac+' Disconnect...' });
	 				 _blocking=true;
	 			 }
			}
		} );
		ds_socket.on('broadcast_room', function (result) {
					exec(result);
		});
}
var _test;
var _re_loading=false;
function exec(info){
	var from=info.from;
	var cmd=info.data.cmd;
	var raw_data=info.data.data;

	//console.log(info);
	if (cmd=='IM_BOARD_SERVER'){
	   var xmac=info.data.data.mac;
	   var sinfo=info.data.data.server_info;
	   if (! _dice_servers[xmac]){
			 _dice_servers[xmac]=sinfo;
			 //console.log(sinfo);
			 $("#dice_servers").append("<option class='dice_servers' value='"+xmac+"'>"+
         Object.keys(_dice_servers).length+"."+(sinfo.name?sinfo.name:sinfo.ip)+
				 "</option>");
			 $("#dice_servers").selectpicker('refresh');
			 hideone("#dice_servers");
		}

		showState('rgba(0, 255 , 0, 0.4)','fa fa-eye fa-3x','Dice Server \n'+JSON.stringify(Object.keys(_dice_servers))+'\n已連線');
		if (! _islogin && ! _re_loading && _save_login && _choiced && _choiced.mac==xmac){
			load_session();
			re_login();
			_re_loading=true;
		}


		if (_blocking){
			 $.unblockUI;
			 _blocking=false;
			 $.unblockUI();
			 $('html,body').css('cursor','default');
		}
		_ds_socket.emit("monitor_it",{mac:xmac,events:'disconnect'},
				function(result){
		});
		fetch_dice_server(xmac);
	} else if (cmd=='ttyd'){
		var act=raw_data['act'];  //{act:<act>[,para]}
		if (act=='hello'){
		    //board_ttyd.js
			_ttyd_loaded=true;
			_ttyd_mac=from;
			//ttyd_term_fortune();
		} else if (act=='connect_opened'){
			_ttyd_loaded=true;
			//jj_notify("info", "終端機開啟!");
			console.log( "終端機開啟!");
			$("#ttyd_loading").show();
			$("#ttyd_c").hide();
		} else if (act=='connect_reconnect'){
			//jj_notify("info", "終端機連線中!");
			console.log( "終端機連線中!");
			if (_user_info.st_name){
				ttyd_set_prompt("["+_user_info.st_name+"@"+_user_info.cl_no+"] $" );
			}
			ttyd_term_fit();
			ttyd_term_cursorBlink();
			ttyd_term_clear();
			$("#ttyd_loading").hide();
			$("#ttyd_c").show();
		} else if (act=='connect_broken'){
			_ttyd_loaded=false;
			//jj_notify("info", "終端機中斷!");
			//ttyd_reload(true);
			console.log( "終端機中斷!");
			$("#ttyd_loading").show();
			$("#ttyd_c").hide();
		} else if (act=='connect_broken_bycode'){
			_ttyd_loaded=false;
			//ttyd_reload(true);
			//jj_notify("info", "終端機正常中斷!");
			console.log( "終端機正常中斷!");
			$("#ttyd_loading").show();
			$("#ttyd_c").hide();
		} else if (act=='python_error'){
		    var data=raw_data['para']['message'];
			var ldata=data.split(" ");
			if (ldata.length>=6){
				var r=ldata[5];
				//if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
				_exec_err.push({
					  row: r-1,
					  column: 0,
					  text: data,
					  type: 'error'
				});
				_ace.getSession().setAnnotations(_exec_err);
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
			}
		} else if (act=='lua_error'){
		    var data=raw_data['para']['message'];
			var ldata=data.split(":");
			if (ldata.length>=3){
				var r=ldata[2];
				if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
				_exec_err.push({
					  row: r-1,
					  column: 0,
					  text: data,
					  type: 'error'
				});
				_ace.getSession().setAnnotations(_exec_err);
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
			}
		} else if (act=='ruby_error'){
		    var data=raw_data['para']['message'];
			var ldata=data.split(":");
			if (ldata.length>=2){
				var r=ldata[1];
				if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
				_exec_err.push({
					  row: r-1,
					  column: 0,
					  text: data,
					  type: 'error'
				});
				_ace.getSession().setAnnotations(_exec_err);
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
			}
		}else if (act=='perl_error'){
		    var data=raw_data['para']['message'];
		    var s=data.indexOf('at main.pl line');
			if (s>=0){
				s=s+16;  //16: length of 'at main.pl line '+1
				var e=data.indexOf('.', s);
				var r=data.substring(s,e).trim();
				if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
				_exec_err.push({
					  row: r-1,
					  column: 0,
					  text: data,
					  type: 'error'
				});
			}
			_ace.getSession().setAnnotations(_exec_err);
			//ttyd_term_showOverlay('程式有誤: '+data,1000);
			//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");

		}  else if (act=='php_error'){
		    var data=raw_data['para']['message'];
			var ldata=data.split(" ");
			var r=ldata[ldata.length-1];
			r=(r>_ace.getSession().getLength())?_ace.getSession().getLength():r;
			if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
			_exec_err.push({
				  row: r-1,
				  column: 0,
				  text: data,
				  type: 'error'
			});
			_ace.getSession().setAnnotations(_exec_err);
			//ttyd_term_showOverlay('程式有誤: '+data,1000);
			//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");

		}  else if (act=='node_error'){
		    var data=raw_data['para']['message'];
			var adata=data.split("\n");
			for (var i=0;i<adata.length;i++){
				if (adata[i].includes('/main.js:')){
					var ldata=adata[i].split(":");
					if (ldata.length>=2){
						var r=ldata[1];
						if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
						_exec_err.push({
							  row: r-1,
							  column: 0,
							  text: data,
							  type: 'error'
						});
						_ace.getSession().setAnnotations(_exec_err);
						//ttyd_term_showOverlay('程式有誤: '+data,1000);
						//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
					}
					break;
				}
			}
		}  else if (act=='java_error'){
		    var data=raw_data['para']['message'];
			var ldata=data.split(":");
			if (ldata.length>=2){
				var r=ldata[1];
				if (_full_sound=="1") $.speak('我想程式第'+r+'行有問題喔！');
				_exec_err.push({
					  row: r-1,
					  column: 0,
					  text: data,
					  type: 'error'
				});
				_ace.getSession().setAnnotations(_exec_err);
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
			}
		} else if (act=='gcc_error'){
			var data=raw_data['para']['message'];
			var ldata=data.split("\n");
			var elines='';
			for (var i=0;i<ldata.length;i++){
				var adata= ldata[i].split(':');
				if (adata.length>=5){
					var r=adata[1],c=adata[2],type=adata[3].trim();
					var info=adata[4];
					_test=info;
					if (type=='error') type='error';
					else if (type=='warning') type='warning';
					else type='info';
					elines=elines+', '+r;
					_exec_err.push({
						  row: r-1,
						  column: c-1,
						  text: info,
						  type: type
					});
					_ace.getSession().setAnnotations(_exec_err);
				}
			}
			if (ldata && ldata.length>0){
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
				if (_full_sound=="1") $.speak('我想程式第 '+elines+' 行有問題喔！');
			}
		}  else if (act=='c#_error'){
			var data=raw_data['para']['message'];
			var ldata=data.split("\n");
			var elines='';
			for (var i=0;i<ldata.length;i++){
				var adata= ldata[i].split(':');
				if (adata.length>=3){
					var rc=adata[0].substring(8,adata[0].length-1).split(',');
					var r=rc[0],c=rc[1],type=adata[1].trim().split(' ')[0];
					var info=adata[2];
					if (type=='error') type='error';
					else if (type=='warning') type='warning';
					else type='info';
					elines=elines+', '+r;
					_exec_err.push({
						  row: r-1,
						  column: c-1,
						  text: info,
						  type: type
					});
					_ace.getSession().setAnnotations(_exec_err);
				}
			}
			if (ldata && ldata.length>0){
				//ttyd_term_showOverlay('程式有誤: '+data,1000);
				//ttyd_term_send("cowthink ''我想程式有問題喔！\x0D");
				if (_full_sound=="1") $.speak('我想程式第 '+elines+' 行有問題喔！');
			}
		}
    }  else if (cmd=='notification'){
			if (from!=_choiced_mac) return;
		if (raw_data.type=='login'){  //Somebody login
			var login_user=raw_data.data;
			var login_mac=raw_data.mac;
			if (!_login_users) _login_users={};
			_login_users[login_mac]=login_user;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
			  $("[name='stu_"+login_user.id+"']").css('background-color', 'rgba(135, 199, 135,1)');
				if ( _students && _students[login_user.id]) _students[login_user.id].isLogin=true;
				if ( _watch_all !='N' && (_watch_all=='Y' || ( login_mac!=_mac && _full_anchor=="full_login")
						|| ( _students && _students[login_user.id]
						&& _students[login_user.id].isWatch)) ){
					jj_notify("info",
							'使用者 '+login_user.name+'@'+login_user.class+' 登入',true);
				}
				var times=parseInt($("#inf_"+login_user.id).text())+1;
				$("#inf_"+login_user.id).text(times);
			}
		}  else if (raw_data.type=='reload_dice_server'){
			jj_notify("warn", "主機發起: 重載DICE Server!!");
			//reload_dice_server();
			location.reload();
		}	else if (raw_data.type=='logout'){
			var logout_user=raw_data.data;
			var logout_mac=raw_data.mac;

			if (_user_info && _user_info.clientInfo==logout_mac){
				jj_notify("warn", "主機發起: 登出！");
				location.reload();
			}
			if ( _watch_all !='N' &&
				  ( logout_user
						&& _watch_all==='Y'
				    || (logout_mac !=_mac && _full_anchor==="full_login" )
					  || (_students && _students[logout_user.id] &&
							_students[logout_user.id].isWatch)
					)
				){
				jj_notify("warn",
						'使用者 '+logout_user.name+'@'+logout_user.class+' 登出',true);
			}
			if (logout_user){
				var times=parseInt($("#inf_"+logout_user.id).text())-1;
				if (times<=0){
					$("[name='stu_"+logout_user.id+"']").css('background-color', 'rgba(135, 199, 135,0.5)');
					_students[logout_user.id].isLogin=false;
				  times=0;
				}
				$("#inf_"+logout_user.id).text(times);
			}
			delete _login_users[logout_mac];

		}  else if (raw_data.type=='case_save'){
			var uname=raw_data.uname;
			var caseid=raw_data.caseid;
			var desc=raw_data.desc;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
						+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')已修改');
				_exercises[uname].teachingunit[parseInt(caseid)][8]=desc;
				$("div#tcase_"+uname+"_"+(parseInt(caseid)+1)).webuiPopover('destroy');
				$("div#tcase_"+uname+"_"+(parseInt(caseid)+1)).attr('data-content',desc);
				$("div#tcase_"+uname+"_"+(parseInt(caseid)+1)).webuiPopover({placement:'auto',
					trigger:'hover',
					width:'500px',
					height:'250px',
					animation:'pop',
					delay: {
						show: 2000,
				        hide: 300
				    },
					closeable:true});
				if (! _user_info.isTeacher)
					$("div#cont_"+uname+"_"+(parseInt(caseid)+1)).html(desc);
				}  else if (raw_data.type=='add_unit_before'){
					var uname=raw_data.uname;
					if (_user_info.isTeacher){
						jj_notify('success','已加至入單元 '+uname+'之前');
						//reload_exercises();
				}
			}
		} else if (raw_data.type=='add_unit_after'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('success','已加至入單元 '+uname+'之後');
					//reload_exercises();
				}
			}
		}  else if (raw_data.type=='append_case'){
			//var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('success','新題目已加至入單元'+raw_data.udesc+', '+raw_data.case_name+'之前');
					//reload_exercises();
				}
			}
		}  else if (raw_data.type=='unit_lock'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				jj_notify('error','單元 '+raw_data.name+'已鎖定');
				//if (! _user_info.isTeacher)
				unit_lock(uname);
			}
		} else if (raw_data.type=='unit_unlock'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				jj_notify('success','單元 '+raw_data.name+'已解鎖');
				//if (! _user_info.isTeacher)
				unit_unlock(uname);
			}
		} else if (raw_data.type=='remove_unit'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('error','單元 '+raw_data.name+'已刪除');
				}	else {
					jj_notify('error','單元 '+raw_data.name+'已經被老師刪除');
				}

				if (_exercises[uname]){
				   delete _exercises[uname];
					_exercise_keys.splice(_.indexOf(_exercise_keys,uname),1);
					redraw_exercises(false);
					unit_remove(uname);
				}

			}
		}  else if (raw_data.type=='remove_case'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('error','單元: '+raw_data.udesc+'題目: '+raw_data.case_name+'已刪除');
				}	else {
					jj_notify('error','單元: '+raw_data.udesc+'題目: '+raw_data.case_name+'已經被老師刪除');
					reload_exercises(true,true);
				}
			}
			//console.log(raw_data);

			//reload_exercises(true);
			/*
			delete _exercises[uname];
			_keys.splice(_.indexOf(_keys,uname),1);
			redraw_exercises(false);
			unit_remove(uname);
			*/
		} else if (raw_data.type=='reload_exercises'){
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (!_user_info.isTeacher){
					jj_notify('warning','老師推送新題組, 題組更新中...');
					reload_exercises(true,true);
				}
			}
		} else if (raw_data.type=='unit_bin'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('error','單元 '+raw_data.name+'已隱藏');
					unit_bin(uname);
				}	else {
					jj_notify('error','單元 '+raw_data.name+'已經被老師隱藏');
					unit_bin(uname);
				}
			}
		} else if (raw_data.type=='unit_save'){
			var all=raw_data.all;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				jj_notify('success','單元 '+all.unitName+'已修正');
			}
			//$('#ex_'+all.unitName).text(all.unitDescription);
			/*
			_dice_server[_choiced_mac]['teachingcycles'][_choiced_teachers]
			  ['exercises'][all.unitName].all=all;
			  */
		} else if (raw_data.type=='case_save_input'){
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					var uname=raw_data.uname;
					var caseid=raw_data.caseid;
					var desc=raw_data.desc;
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
							+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')輸入測資已儲存');
					//reload_exercises(true);
				}
			}

		} else if (raw_data.type=='case_save_output'){
			if (_user_info.isTeacher){
				var uname=raw_data.uname;
				var caseid=raw_data.caseid;
				var desc=raw_data.desc;
				var cl_no=raw_data.cl_no;
				var key=raw_data.key;
				if (cl_no==_choiced_cl_no && key==_choiced_teachers){
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
							+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')輸出答案已儲存');
					}
				   //reload_exercises(true);
				}

		} else if (raw_data.type=='case_save_constraint'){
			if (_user_info.isTeacher){
				var uname=raw_data.uname;
				var caseid=raw_data.caseid;
				var desc=raw_data.desc;
				var cl_no=raw_data.cl_no;
				var key=raw_data.key;
				if (cl_no==_choiced_cl_no && key==_choiced_teachers){
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
						+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')解題限制已儲存');
				}
				//reload_exercises(true);
			}

		} else if (raw_data.type=='case_ref_ans'){
			if (_user_info.isTeacher){
				var uname=raw_data.uname;
				var caseid=raw_data.caseid;
				var desc=raw_data.desc;
				var cl_no=raw_data.cl_no;
				var key=raw_data.key;
				if (cl_no==_choiced_cl_no && key==_choiced_teachers){
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
						+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')參考答案已儲存');
				}
				//reload_exercises(true);
			}

		} else if (raw_data.type=='case_save_title'){
			if (_user_info.isTeacher){
				var uname=raw_data.uname;
				var caseid=raw_data.caseid;
				var desc=raw_data.desc;
				var cl_no=raw_data.cl_no;
				var key=raw_data.key;
				if (cl_no==_choiced_cl_no && key==_choiced_teachers){
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
							+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')名稱已更改');
				   $("#case_name").text(desc);
				}
				//reload_exercises(true);
			}
			_exercises[uname].teachingunit[parseInt(caseid)][0]=desc;
		} else if (raw_data.type=='case_save_score'){
			if (_user_info.isTeacher){
				var uname=raw_data.uname;
				var caseid=raw_data.caseid;
				var desc=raw_data.desc;
				var cl_no=raw_data.cl_no;
				var key=raw_data.key;
				if (cl_no==_choiced_cl_no && key==_choiced_teachers){
					jj_notify('info','單元'+uname+'('+raw_data.udesc+') 第'
							+(parseInt(caseid)+1)+'題 ('+raw_data.case_name+')相對分數('+desc+')已儲存');
				   $("#case_score").text('('+desc+')');
				}
			}
			_exercises[uname].teachingunit[parseInt(caseid)][2]=desc;
		} else if (raw_data.type=='unit_unbin'){
			var uname=raw_data.uname;
			var cl_no=raw_data.cl_no;
			var key=raw_data.key;
			if (cl_no==_choiced_cl_no && key==_choiced_teachers){
				if (_user_info.isTeacher){
					jj_notify('success','單元 '+raw_data.name+'已經顯示');
					unit_unbin(uname);
				} else {
					jj_notify('success','單元 '+raw_data.name+'已經老師顯示');
					unit_unbin(uname);
				}
			}
		}
	} else if (cmd=='judge_result_others'){
		var score=info.data.data.info.message.score;
		var user_info=info.data.data.info.message.user_info;
    if (_choiced_cl_no!==user_info.cl_no) return;
		var sp=-1;
		$.each(_total_scores.data,function(i,v){
			if (v._id===user_info.st_no){
				v[user_info.ex_no]=user_info.total;
				sp=i;
				var tot=0;
				$.each(v,function(k,s){
					 if (!k.startsWith('_')){
						 	tot+=parseFloat(s);
					 }
				});
				//console.log(tot);
				v._total=tot;
				return false;
			}
		});
		//for stacks charts _ total
		var panel=getStackPanel();
		if (panel){
			 var mc=panel['mc'];
			 if (mc){
				 let n=_exercises[user_info.ex_no]['serial']+"."
							 +_exercises[user_info.ex_no]['name'];
				 var d=mc.getOption()['series'][_exercises[user_info.ex_no]['serial']-1].data;
				 if (sp>=0){
					 d[sp]=parseFloat(user_info.total).toFixed(0);
					 mc.setOption({
						series: [{
								name: n,
								type: 'bar',
								stack: 'total',
								label: {
										normal: {
												show: true,
												position: 'insideRight'
										}
								},
								data: d
						}]
					});
				}
			 }
	  }
		//for stacks charts _ unit
    let serial=_exercises[user_info.ex_no]['serial'];

		$.each(_scores[serial-1].table.data,function(i,v){
			if (v._id===user_info.st_no){
				v[user_info.ex_num-1]=score;

				sp=i;
				return false;
			}
		});
		var panel=getStackPanel(_exercises[user_info.ex_no]['serial']);
		if (panel){
			 var mc=panel['mc'];
			 if (mc){
				 let i=user_info.ex_num;
				 let n=(i)+"."+_exercises[user_info.ex_no]['teachingunit'][i-1][0];
				 var d=mc.getOption()['series'][i-1].data;
				 if (sp>=0){
					 d[sp]=parseFloat(score).toFixed(0);
					 mc.setOption({
							series: [{
									name: n,
									type: 'bar',
									stack: 'total',
									label: {
											normal: {
													show: true,
													position: 'insideRight'
											}
									},
									data: d
							}]
						});
					}
			 }
	  }
		//end of stack charts
		//update total scores

		$('#table_'+serial).bootstrapTable('updateByUniqueId',
				{id: user_info.st_no, row: {
			     [parseInt(user_info.ex_num)-1]:parseFloat(score).toFixed(0)
			}});
		$('#table_'+serial).bootstrapTable('updateByUniqueId',
				{id: user_info.st_no, row: {
			     _total:parseFloat(user_info.total).toFixed(0)+''
			}});
		$('#table_sco').bootstrapTable('updateByUniqueId',
				{id: user_info.st_no, row: {
					[user_info.ex_no]:parseFloat(user_info.total).toFixed(0)+''
			}});



	try {
       var s_tab = $.grep(_total_scores.data, function(e){ return e._id == user_info.st_no; });

       if (s_tab && s_tab.length==1){
            //console.log(s_tab[0]);
            var tot=0;
            $.each(s_tab[0],function(s,st){
                    if (! s.startsWith("_")){
                            tot+=parseFloat(st);
                    }
            });
            $('#table_sco').bootstrapTable('updateByUniqueId',
                            {id: user_info.st_no, row: {
                         _total:tot.toFixed(0)
            }});
            $('#bsco_'+user_info.st_no).html(tot.toFixed(0)); //show in wall
       }
       var ggname="XXX";
       var csname="OOO";
       if (_dice_server[_choiced_mac].
                    teachingcycles[_choiced_teachers].exercises[user_info.ex_no]){
           ggname=_dice_server[_choiced_mac].
             teachingcycles[_choiced_teachers].exercises[user_info.ex_no].name;
       }
       if (_dice_server[_choiced_mac].teachingcycles[_choiced_teachers]
           .exercises[user_info.ex_no] && _dice_server[_choiced_mac].teachingcycles[_choiced_teachers]
           .exercises[user_info.ex_no].teachingunit[user_info.ex_num-1]){
            csname=_dice_server[_choiced_mac].teachingcycles[_choiced_teachers]
                    .exercises[user_info.ex_no].teachingunit[user_info.ex_num-1][0];
       }
    } catch (e){console.log(e);}

		if ( _watch_all !='N' && (_watch_all=='Y' || _full_slide_anchor=="total_scores" ||
				_full_slide_anchor=="users_wall"
			|| (! _user_info.isTeacher &&  _students[user_info.st_no].isWatch))
			){
			  jj_notify("info", user_info.st_name+'('+user_info.st_no+')@'+user_info.cl_no
					+'解 '+ggname+'-'+csname+'得'+parseFloat(score).toFixed(0)+'分,'+'目前總分:'
					+user_info.total.toFixed(0),true
			);
		}
	} else if (cmd=='judge_result'){
		var xinfo=info.data.data.info;
		if (xinfo.result==1){
			var xmessage=xinfo.message.message;
			var xscore=xinfo.message.score;
			//_users.scores[_choiced_question.j].score[_choiced_question.i+1]=xinfo.message;
			$('#plab_'+_choiced_question.j+'_'+(_choiced_question.i+1)+" .blue-btn .first-link").text(
					'('+parseFloat(xscore).toFixed(1)+')'
					+(_choiced_question.i+1)
					+escapeHtml(_choiced_question.tcase[0])

			);
			gotocase(_choiced_question.j,_choiced_question.i+1);
			$('#pbar_'+_choiced_question.j+'_'+(_choiced_question.i+1)).
			progressbar('value',parseFloat(xscore));
			execute('get_user_code',_choiced_mac,
					_choiced_question.para,
					function(info){
						if (_log_comm && _log_comm.is_plugin){
							_log_comm.broadcast('monitor', {comm:'judge_result',data:{
								user_info:_user_info,
								info:info,
								question:_choiced_question,
								score:xscore,
								message:xmessage
							}});
						}
						_cache['#pbar_'+_choiced_question.j+'_'+(_choiced_question.i+1)]=info;
					     handle_answers(info,_choiced_question.unit_name);
			});
		} else if (xinfo.result<=0){
			//console.log(xinfo);
			var xmessage=xinfo.message.message;
			//block(xmessage,3000);
		}
		if (xmessage)
			$.blockUI({
				message:xmessage,
	            onOverlayClick: $.unblockUI,
	            css: {
	                width: '70%',
	                position: 'absolute',
	                top: '50%',
	                left: '50%',
	                transform: 'translate(-50%, -50%)',
	                padding: '5px',
	                backgroundColor: '#ffffff',
	                '-webkit-border-radius': '10px',
	                '-moz-border-radius': '10px',
	                opacity: .8,
	                color: '#000000'
	            }
	        });
	}
}
var _logout=false;
function logout(noclear){
	if (! _islogin) return;
    $('html,body').css('cursor','wait');
    $.blockUI('請稍待');
    flush_ace_logs(true);
	execute('logout',_choiced_mac,{},
		function(info){
		  $.unblockUI();
		  $('html,body').css('cursor','default');
			if (info.result>=0){
				//sessionStorage.remove('_save_login');
				//sessionStorage.remove('_exercises_info');
				//sessionStorage.remove('_class_score');
				//sessionStorage.remove('_save_scores');
				if (! noclear) sessionStorage.clear();
				_logout=true;
				location.reload();
			} else {
				block("登出無法完成!",1000);
				 if (! $(".button-wrap").hasClass("button-active")){
					  $(".button-wrap").toggleClass('button-active');
				  }
			}
	});
	return 0;
}
var _pwd;
var _watch_list;
var _materials={};
var _log_comm,_log_doing=false,_class_room;
function re_login(){
		if (_save_login){
			 exec_login(_save_login.id,_save_login.pwd);
		}
}
function exec_login(id,pwd,save){
	_pwd=pwd;
	$('html,body').css('cursor','wait');
	$.blockUI('請稍待');
	if (save){
	   _save_login=save;
	} else {
	   _save_login={id:id,pwd:pwd,cl_no:_choiced_cl_no,base:_choiced_teachers,
			 hash:""+(_choiced_mac+_choiced_cl_no+_choiced_servers+_choiced_teachers).hashCode()};
	}
	execute('login',_choiced_mac,
			_save_login,
			function(info){
				$.unblockUI();
				if (! save){
					$('html,body').css('cursor','default');
					if (info.result>=0){

					//Passed
					_users=info.data.data;

					_login_users=_users.login_users;
					_user_info=_users.user_info;
					_watch_list=_users.watch_list;
					var scores=_users.scores;
					if (_users.result==1){  //Passed
           			let envi=_users.envi;
					if (envi){
						_choiced_question=envi.question;
						var full=envi.full;
					  if (full){
					      _bk_r=full.index;_bk_c=full.slide_index;
					      _gotolast=true;
					  }
					  if (envi.ace){
							sessionStorage.setItem('_editorSession', JSON.stringify(envi.ace));
							jsonToSession(_ace,envi.ace);
							_re_loading=true;
					  }
				  }

				_islogin=true;
				_exec_language=_exec_language_n[_user_info.cl_no+_user_info.st_no];
				if (! _exec_language){
					_exec_language="C";
					_exec_language_n[_user_info.cl_no+_user_info.st_no]=_exec_language;
					Cookies.set('_exec_language_n', _exec_language_n);
				}
				$('#langMenu').html(_exec_language + ' <span class="caret"></span>');
				$('#langMenu').val(_exec_language);

				_exec_theme=_exec_theme_n[_user_info.cl_no+_user_info.st_no];
				if (! _exec_theme){
					_exec_theme='tomorrow_night_bright';
					_exec_theme_n[_user_info.cl_no+_user_info.st_no]=_exec_theme;
					Cookies.set('_exec_theme_n', _exec_theme_n);
				}
				$('#langTheme').html(_exec_theme + ' <span class="caret"></span>');
				$('#langTheme').val(_exec_theme);

				$(document).prop('title', 'Dice-'+_user_info.st_name+'@'+_user_info.cl_no);
				/* TODO_jj_20170823_1: 教學資源管理 (Ref: full.html)*/
				/*
				execute('get_material_dir',_choiced_mac,
						{"key":_choiced_teachers,"cl_no":_choiced_cl_no},
						function(result){
							$('#cm_head').text(_user_info.cl_no+'班級教學資源');
							_materials=result.data.data.dir;
							//var data=[{"title": "Node 1", "key": "1"}, {"title": "Folder 2", "key": "2", "folder": true, "children": [ {"title": "Node 2.1", "key": "3"}, {"title": "Node 2.2", "key": "4"} ]} ];
							//console.log(_materials);
							$("#fancy_tree").fancytree({source:_materials});
							//$("#fancy_tree").fancytree();
							//$("#fancy_tree").fancytree("option", "source", _materials);
							//console.log(_materials);
						}
				);
				*/
					$('#ed_his').hide();
					$('#rm_his').hide();
					$('#push_clients').hide();
					$('#tc_export').hide();
					$('#tc_import').hide();
					$('#tc_pdf').on("click",
					function(e){
						e.preventDefault();
						e.stopPropagation();
						tc_print_pdf($(this));
					});

						if (_user_info.isTeacher){
							 $('#ed_his').show();
							 $('#rm_his').show();
							 $('#push_clients').show();
							 $('#tc_export').show();
							 $('#tc_import').show();

							 $('#ed_his').on("click",
							    		function(){
							    			ed_his();
							  });
							 $('#rm_his').on("click",
							    		function(){
							    			rm_his();
							  });
							 $('#push_clients').on("click",
							    		function(){
							    			push_clients();
							  });
							 $('#tc_export').on("click",
							    		function(e){
							    			tc_export(e);
							  });
							 $('#tc_import').on("click",
							    		function(e){
							    			tc_import(e);
							  });

							_full_all=true;
							load_user_mange_head();     //in mange_users.js

							//teacher _nav_items
							_nav_items[2].children.push({
								href: 'javascript:$.fn.fullpage.moveTo("full_ques",4);',
								text: '解析樹'
							});
							_nav_items[2].children.push({
								href: 'javascript:$.fn.fullpage.moveTo("full_ques",5);',
								text: '流量監看'
							});
						} else {
							load_user_head();
						}
						_nav_items.push({   //_nav_items[_nav_items.length-1]
							href: '#nav_testcase',
							text: '測試單元',
							children:[]
						});
						ask_exercises(   //in board_socketio.js
								false,
								false,
								false,
								function(){
									if (_user_info.isTeacher){
										jj_notify("warn", "歡迎  "+ _user_info.st_name+'@'+_user_info.cl_no
												+"  老師登入"
										);
										if (_full_sound=="1") $.speak( "歡迎  "+ _user_info.st_name+"  老師登入");
									} else {
										jj_notify("warn", "歡迎  "+ _user_info.st_name+'@'+_user_info.cl_no
												+"  同學登入"
										);
										if (_full_sound=="1") $.speak( "歡迎  "+ _user_info.st_name+"  同學登入");
									}
								}
						);

						$('#loginhead').text('登出');
						$('#loginnow').text('點我登出');
						$('#loginnow').attr('act','logout');
						$("#id").hide();
						$("#pwd").hide();
						$("#cl_no").selectpicker('hide');
						$("#dice_servers").selectpicker('hide');
						$("#servers").selectpicker('hide');
						$("#teachers").selectpicker('hide');



						if ($('#fullpage')[0]){
							$.fn.fullpage.moveTo("full_ques",2);
							//ttyd_reload(false);
						}
						//For FB bind
						//fb_login();
						if (_is_FB){
							init_FB();
						}

						if (! _log_doing){
							let comm_str1=`let key=data.data.data.id
									,mac=data.data.data.mac;
									if (! _watcher[key]) {
										 _watcher[key]=new Watching(key,mac,_log_comm,{addControls:false,theme:'info'});
										 _watcher[key].panel.options.onclosed.push(()=>{
												 if (_watcher[key]) delete _watcher[key];
										 });
	 									_watcher[key].ace.setValue(data.data.data.value);
									}
							`;
							var commands={
						    user_login: `if (!this.users[data.room]) this.users[data.room]={};
						                this.users[data.room][data.from]=data.data.data.data;
						                if (_is_watching) start_monitor(data.from);
						                this.ds_socket.emit("monitor_it",{mac:data.from,events:'disconnect left'},()=>{});`,
						    get_users:`this.send(data.from,{comm:'add_me',room:data.room,data:this.userinfo});`,
						    add_me: `if (!this.users[data.data.data.room]) this.users[data.data.data.room]={};
						                this.users[data.data.data.room][data.from]=data.data.data.data;
						                this.ds_socket.emit("monitor_it",{mac:data.from,events:'disconnect left'},()=>{});`,
						    monitor_off: `if (this.users[data.room][data.from].isTeacher){
						                  if (this.monitors[data.room][data.from]) {
						                    delete this.monitors[data.room][data.from];
						                  }}`,
						    monitor_on: `var room=data.room;
														 if (data.data.data.room) room= data.data.data.room;
														 if (this.users[room][data.from] && this.users[room][data.from].isTeacher){
						                   this.users[room][data.from]['ask_monitor']=true;
						                   if (! this.monitors[room]) this.monitors[room]={};
						                   this.monitors[room][data.from]=true;
						                   this.monitor_enevts=data.data.data.data;
						                   var extra_func=eval(data.data.data.extra);
						                   if (! this.is_monitor_on){
						                      $(window).on(this.monitor_enevts,(e)=>{
						                        var e_info={
						                          class: e.target.className,
						                          id: e.target.id,
						                          altKey: e.altKey,
						                          ctrlKey: e.ctrlKey,
						                          type: e.type,
						                          key: e.key,
						                          keyCode: e.keyCode,
						                          pageX: e.pageX,
						                          pageY: e.pageY,
						                          screenX: e.screenX,
						                          screenY: e.screenY,
						                          offsetX: e.offsetX,
						                          offsetY: e.offsetY,
						                          extra: extra_func(e,data.from)
						                        }
						                        $.each(this.monitors[room],(k,v)=>{
						                          this.send(k,{comm:'got_mon_events',room:data.room
						                          ,info: e_info,cbstr: data.data.data.cbstr});
						                        });
						                      });
						                      this.is_monitor_on=true;
						                   }
						                }`,
						    got_mon_events: `eval('var cbf='+data.data.data.cbstr);
						                     eval(cbf(data.data.data.room,data.data.data.info));
						                     `,
								share_ace_action: `_ace.setValue(data.data.data.value);
																	 _ace.selection.moveTo(data.data.data.end.row,data.data.data.end.column);
																	`,
								broadcast_ace_action: `if (data.data.data.sender!==_user_info.st_no){`
																			+comm_str1+`
																			else {_watcher[key].ace.setValue(data.data.data.value);}
																	 	 }`,
							  open_class_chat: `if (data.data.data.sender!==_user_info.st_no){`
																		 +comm_str1+`
																		 _watcher[key].open_class_chat();
																 }`,
							  open_user_chat: `if (data.data.data.sender!==_user_info.st_no){`
																		 +comm_str1+`
																		 _watcher[key].open_user_chat();
																 }`,
							  broadcast_class_chat: `if (data.data.data.sender!==_user_info.st_no){`
																		 +comm_str1+`
																		 _watcher[key].send_to_class_chat(
																			 {user:data.data.data.msg.user,
																			 msg:data.data.data.msg.msg});
																 }`,
							  send_user_chat: `if (data.data.data.sender!==_user_info.st_no){`
																		 +comm_str1+`
																		 _watcher[key].send_to_user_chat(
																			 {user:data.data.data.msg.user,
																			 msg:data.data.data.msg.msg});
																 }`,
								notify: `jj_notify(data.data.data.type,data.data.data.info,true);`,
								moveto: `$.fn.fullpage.moveTo(data.data.data.r, data.data.data.c);`,
								click_on_ace: `$('.ace_text-input').click();`
						  }
							_log_comm=new D_COMM(''+(_choiced_teachers+_choiced_cl_no).hashCode(),_ms_url,commands);
							_class_room='room'+(_choiced_teachers+_choiced_cl_no).hashCode();
							_log_comm.connect(()=>{
								_log_comm.join('monitor',_user_info);
								_log_comm.join(_class_room,_user_info);
							});
							/*
							await _log_comm.join('monitor',_user_info);
							_class_room='room'+(_choiced_teachers+_choiced_cl_no).hashCode();
							await _log_comm.join(_class_room,_user_info);
              */
							//await	_log_comm.broadcast('monitor', {comm:'user_login',data:_user_info});
							//await	_log_comm.broadcast(_class_room, {comm:'user_login',data:_user_info});
							_log_doing=true;
						}

					} else {
						//TODO: Not Passed
						block("登入失敗!",3000,"請重新輸入帳號密碼, 注意大小寫!");
						if (_full_sound=="1") $.speak("登入失敗!, 請重新輸入帳號密碼, 注意大小寫!");
						if ($(".button-wrap").hasClass("button-active")){
							  $(".button-wrap").toggleClass('button-active');
						 }
						return -1;
					}
				} else {
					block("系統錯誤!",3000,JSON.stringify(info.data));
					return -1;
				}
		   }
		});
	  return 0;
}


var _total_scores={};
var _scores={};
function get_class_score(force,cb){
	let cs_info=sessionStorage.getItem('_class_score');
	if (! cs_info || force){
		execute('get_class_score',_choiced_mac,
				{cl_no:_choiced_cl_no,key:_choiced_teachers,pwd:_pwd},
				function(info){
					sessionStorage.setItem('_class_score',JSON.stringify(info));
					handle_class_score_info(info,cb);
		});
	} else {
		let info=JSON.parse(cs_info);
		if (sessionStorage.getItem('_save_scores')){
			info.data.data.scores=JSON.parse(sessionStorage.getItem('_save_scores'));
		}
		if (sessionStorage.getItem('_total_scores')){
			_total_scores=JSON.parse(sessionStorage.getItem('_total_scores'));
		}
		handle_class_score_info(info,cb);
	}
}

function handle_class_score_info(info,cb){
	_total_scores={};
	if (info.data.data.result>=0){
		var scores=info.data.data.scores;
		_scores=scores;
		var columns=[{field:"_id",sortable:true,title:"ID",},
						{field:"_name",sortable:true,title:"姓名"}];
		var data={};

		tdata={};
		var c=0;
		var sc=Object.keys(scores).length;
		$.each(scores,function(i,score){
			let jtit=score._id;
			if (_exercises[score._id]){
				jtit=_exercises[score._id].serial;
			}
			columns.push({field:score._id,sortable:true,title:jtit});
			data=score.table.data;
			c++;
			$.each(data,function(ii,datum){
				var id=datum._id;
				if (!tdata[id]) {
					tdata[id]={};
					tdata[id]['_id']=id;
					tdata[id]['_name']=datum._name;
					tdata[id]['_total']=0;
				};
				tdata[id][score._id]=datum._total;
				tdata[id]['_total']+=
					(datum._total=='NaN'?0:parseFloat(datum._total));

				if (c>=sc){
					tdata[id]['_total']=Math.round(tdata[id]['_total']).toFixed(2);
				}
			});

			score.table['exportDataType']='all';
			score.table['locale']=_i18n_lang;

			$('#table_'+_exercises[scores[i]._id].serial).bootstrapTable(score.table);
			$('#s_'+_exercises[score._id].serial+" [name='refresh']").off('click');
			$('#s_'+_exercises[score._id].serial+" [name='refresh']").click(function() {
					get_class_score(true,cb);
					//console.log('#s_'+score._id);
			});


			$(function () {
				$('#toolbar_'+_exercises[scores[i]._id].serial).find('select').change(function () {
					score.table['exportDataType']=$(this).val();
					$('#table_'+_exercises[scores[i]._id].serial).bootstrapTable('destroy').bootstrapTable(score.table);
				});
			})
		});
		columns.push({field:"_total",sortable:true,title:"總分",align:"right"});
		_total_scores['columns']=columns;
		_total_scores['locale']=_i18n_lang;
		_total_scores['stickyHeader']=true;
		_total_scores['stickyHeaderOffsetY']='60px';
		var cdata=[];
		$.each(tdata,function(k,v){
			cdata.push(v);
		})
		_total_scores['data']=cdata;
		$('#total_sco_body').html(get_totsco_content());

		$('#table_sco').bootstrapTable(
				_total_scores
		);
		$("#total_sco_body [name='refresh']").off('click');
		$("#total_sco_body [name='refresh']").click(function() {
				get_class_score(true,cb);
		});
	}
	if (cb) cb();
}


function join(room){
	_ds_socket.emit("join",room,
	function(result){
		//console.log("join room "+room+" result: "+JSON.stringify(result)+"");
		where_is_board_server();
	});
}

function where_is_board_server(){
	_ds_socket.emit("broadcast_room",
			"board",{cmd:'where_is_board_server',data:{}},
			function(){
	    }
	);
}

function execute(cmd,xmac,para,callback,to_room){
	if (to_room){
		_ds_socket.emit('send_to',
				xmac,
				{cmd:cmd,data:para},
				callback,to_room
		);
	} else {
		_ds_socket.emit('send_to',
				xmac,
				{cmd:cmd,data:para},
				callback
		);
	}
}
var _ip;
function plugin(){
		var info;
		if (_mac) info ={mac:_mac};
		else info={};
		_ds_socket.emit("plugin"
	 			,info
	 			,function(data){
			 		if (data.result==0){
			 			_mac=data.data.mac;
			 			_ip=data.data.ip;
						sessionStorage.setItem('_name',_mac);
			 		}
			 		join('board');
	 	});
}
function reload_dice_server(){
	_cache_ask={};
	fetch_dice_server(_choiced_mac);
}
async function fetch_dice_server(xmac){
	await ask('get_servers','servers',xmac);
	await ask('get_teachers','teachers',xmac);
	//ask('get_cl_no','cl_no',xmac);
}

var _cache_ask={};
function ask(cmd,fname,xmac,para){
	if (! para) para={};
	if (_cache_ask[cmd+xmac+JSON.stringify(para)]){
		var info=_cache_ask[cmd+xmac+JSON.stringify(para)];
		handle_ask_info(cmd,fname,xmac,para,info);
		return 0;
	} else {
		execute(cmd,xmac,para,function(info){
			if (info.result==0 || info.result==1){
				_cache_ask[cmd+xmac+JSON.stringify(para)]=info;
			}
			handle_ask_info(cmd,fname,xmac,para,info);
			return 0;
		});
	}
}

function handle_ask_info(cmd,fname,xmac,para,info){
	if (info.result==0 || info.result==1){
		var cont=info.data.data[fname];
	    if (! _dice_server[xmac]) _dice_server[xmac]={};
	    _dice_server[xmac][fname]=cont;
	    $("#"+fname).empty();
	    var vvi=0,s_tea=_choiced_teachers,s_cl_no=_choiced_cl_no,
	          s_mac=_choiced_mac,s_servers=_choiced_servers;
	    $.each(cont, function (key, value) {
	    	 if (fname=='teachers'){
	    		 var tbase=value[1];
	    		 //preload

	    		 if (vvi==0) _curr_teacher=value;
	    		 vvi=1;
	    		 $("#"+fname).append("<option class='"+fname
		    			 +"' value='"+value[1]+"' tea_id='"+value[0]+"'>"
		    			 +(value[7]=='default_name'||value[7]==''?value[0]:value[7])
	    		 		 +"</option>");
	    	 } else {
	    		 $("#"+fname).append("<option class='"+fname
	    			 +"' value='"+value[0]+"'>"+value[0]+"</option>");
	    	 }
	    	 $("#"+fname).selectpicker('refresh');
	    	 hideone("#"+fname);
	    	 //special for teachers
	    });
	    if (s_tea) _choiced_teachers=s_tea;
	    if (s_cl_no) _choiced_cl_no=s_cl_no;
	    //if (s_mac) _choiced_mac=s_mac;
	    //if (s_servers) _choiced_servers=s_servers;
	    if (fname=='teachers'){
	    	$('#loginnow .k12').css('cursor','default');
			$('#loginnow .k12').text('點我登入，開始寫程式！');
			if (_blocking){
				 _blocking=false;
				 if (_islogin){
					 exec_login(_users.id,_pwd,_save_login);
				 }
				 $.unblockUI;
			   }
	    }
		//console.log("_dice_server: "+JSON.stringify(_dice_server));
	} else {
		//console.log(cmd+" error: "+JSON.stringify(info.data));
	}
}

function ask_exercises(notdraw,gotolast,force,cb){
	$('html,body').css('cursor','wait');
	let exec_info=sessionStorage.getItem('_exercises_info');
	if (! exec_info || force){
		execute('get_exercises',_choiced_mac
			,{"key":_choiced_teachers,
			"isExt":_user_info.isTeacher,"cl_no":_choiced_cl_no,pwd:_pwd},
			function(info){
				sessionStorage.setItem('_exercises_info',JSON.stringify(info));
				handle_exercises_info(info,notdraw,gotolast,cb);
		});
	} else {
		
		let info=JSON.parse(exec_info);
		handle_exercises_info(info,notdraw,gotolast,cb);
	}

}
function handle_exercises_info(info,notdraw,gotolast,cb){
	var xmac=_choiced_mac;
	var key=_choiced_teachers;
	var scores=_users.scores;
	if (info.result==0 || info.result==1){
			//TODO dice_server[<mac>] 保存 <mac> 的 dice_server 資訊
		var cont=info.data.data['exercises'];
		_exercise_keys=info.data.data['keys'];
		if (! _dice_server[xmac]) _dice_server[xmac]={};
		if (! _dice_server[xmac]['teachingcycles']) _dice_server[xmac]['teachingcycles']={};
		if (! _dice_server[xmac]['teachingcycles'][key])
			_dice_server[xmac]['teachingcycles'][key]={};
		if (! _dice_server[xmac]['teachingcycles'][key]['exercises']) {
			_dice_server[xmac]['teachingcycles'][key]['exercises']={};
		}
			if (! _dice_server[xmac]['teachingcycles'][key]['keys']) {
			_dice_server[xmac]['teachingcycles'][key]['keys']={};
		}
		_dice_server[xmac]['teachingcycles'][key]['exercises']=cont;
			_dice_server[xmac]['teachingcycles'][key]['keys']=_exercise_keys;
		if (! notdraw){

			draw_exercises(
						function(){
						if (cb) cb();
							$.unblockUI();
							$('html,body').css('cursor','default');
			});
			$('#exercises').sortable({
				cursor: "move",
				delay: 250,
				forceHelperSize: true,
				forcePlaceholderSize: true,
				helper: "clone",
				opacity: 0.5,
				placeholder: "highlight",
				revert: true,
				start: function(e, ui) {
					$(this).attr('data-previndex', ui.item.index());
				},
				cancel: ".fixed",
				stop: function(event, ui) {
						var newIndex = ui.item.index();
						var oldIndex = parseInt($(this).attr('data-previndex'));
						$(this).removeAttr('data-previndex');
						ch_unit_order(oldIndex,newIndex,$(this));
					}
				}
			);
			var _rb_start="";
			$('.row_body').sortable({
				cursor: "move",
				delay: 250,
				forceHelperSize: true,
				forcePlaceholderSize: true,
				helper: "clone",
				opacity: 0.5,
				revert: true,
				start: function(e, ui) {
					$(this).attr('data-previndex', ui.item.index());
					$(this).addClass('noclick');
					_rb_start=$(this).attr("uname");
				},
				cancel: ".fixed",
				stop: function(event, ui) {
						var newIndex = ui.item.index();
						var oldIndex = parseInt($(this).attr('data-previndex'));
						$(this).removeAttr('data-previndex');
						uname=$(this).attr("uname");
						ch_case_order(oldIndex,newIndex,uname,$(this));
					}
				}
			);
			$('.row').droppable({
				revert: true,
				drop: function(event, ui) {
					//console.log(event);
					//console.log(ui);
					if ($(this).attr("uname") && _rb_start!=$(this).attr("uname")){
						var target = $(this).attr("uname");
						var source = ui.draggable[0];
						var s_uname=$(source).attr('uname');
						var s_case=$(source).attr('case');
						copy_case(s_uname,s_case,target);
					}
				}
			});

			if ((gotolast || _gotolast) && (_bk_r && _bk_r  > 1) ) 
			  $.fn.fullpage.moveTo(_bk_r?_bk_r:_full_index, _bk_c?_bk_c:_full_slide_index);

		}
	} else {
		console.log(cmd+" error: "+JSON.stringify(info.data));
		if (cb) cb();
		$.unblockUI();
		$('html,body').css('cursor','default');his._ds_socket
	}
	//if(cb) cb();

}
function reload_exercises(refull,force_reload){
	_bk_r=_full_index;_bk_c= _full_slide_index;
	_is_load_wall=false;
	_is_total_scores=false;
	refull=(refull)?refull:false;
	force_reload=(force_reload)?force_reload:false;
	init_draw_exercises(refull);
	ask_exercises(   //in board_socketio.sh
			false,
			true,  //go to last slide
			force_reload
	);
}

function redraw_exercises(refull){
	init_draw_exercises(refull);
	draw_exercises();
}
function init_draw_exercises(refull){
	$("#exercises").empty();
	$("#total_sco_body").empty();
	$("#wcontainer").empty();
	$(".dsection").remove();
	if ($('.user_toggle').hasClass('fa-toggle-on')){
		  $('.user_toggle').toggleClass('fa-toggle-on fa-toggle-off');
		  $('.user_toggle').prop("title", "顯示功能");
		  $(".eye_ctrl").hide();
	 }
	if (refull){
		re_full();
	}
	$('#fp-nav').show();
}

var _exercises;

var _nav_tcases=[];
var t_children={};

function draw_exercises(cb){
	init_draw_exercises();
	var xmac=_choiced_mac;
	var key=_choiced_teachers;
	_exercises=_dice_server[_choiced_mac]['teachingcycles'][_choiced_teachers]['exercises'];
	_is_re_full=true;  //是否重載 sections
	if (! _exercises){
		if (_cached_exercises){
			_exercises=_cached_exercises;
			_is_re_full=false;
		} else {
			return;
		}
	} else {
		_cached_exercises=_exercises;
	}
	_last_item_click="";
	_last_case="";
	var exlen=Object.keys(_exercises).length;
	//TODO: progress when load exercises
	//var ii=0;

	var NAV_LENGTH=7;
	var c_nav_x=0;
	var c_nav_y=0;
	var _draw_count=0;
	_nav_tcases[c_nav_x]=[];

	//$.each(_exercises, function(k, v) {
  $.each(_exercise_keys,function(ii,k){
		 var i=ii+1;
		 _exercises[k].serial=i;
		 _exercises[k].loaded=false;
	   var exercise=_exercises[k];
	   var title=_exercises[k].name;
	   var desc=_exercises[k].description;
	   var uname=_exercises[k].unitname;
	   var tcases=_exercises[k].teachingunit;
	   var all=_exercises[k].all;
	   var uid='ex_'+i;
	   var fid='f_'+i;
	   //generate nav_menu for teat cases
	   if (c_nav_y>=NAV_LENGTH){
		   c_nav_y=0;
		   _nav_tcases[++c_nav_x]=[];
	   }
	   t_children[uname]=[{
	          href: 'javascript:$.fn.fullpage.moveTo("d_'+k+'",0);',
	          text: '班成績'
	   }];
	   _nav_tcases[c_nav_x][c_nav_y++]={
	    			href: '#tcd_'+i,
	    			text: '單元'+i+'.'+title,
	    			children: t_children[uname]
	   };
	   var row=$('<div>',{
		       'class': 'row',
		      //'data-toggle': 'collapse',
		      //'data-target': '#rb_'+uname,
		       'title': title,
		       'id': 'unit_r_'+uname,
		       'uname': uname,
		       'col_num': k
	   });
	   //$('#load_ex_progress').css("width","50%");
	   //console.log( $('#load_ex_progress').css("width"));

	   $("#exercises").append(row);
	   var head=$('<div>',{
	       class:'row_head',
	       id: '"rh_'+uname+'"'});

	   row.append(head);

	   head.append('<div id="'+uid
			   +'" class="col-sm-3"'
			   +" data-toggle='collapse'"
		       +" data-target='#rb_"+i+"'"
			   +' style="min-height:28px;">'
			   +i
			   +'<i id="p_'+uid+'" class="fa fa-print fa-fw" title="輸出單元'+i+'報告"></i>'
			   +'<i id="x_'+uid+'" class="fa fa-table fa-fw" title="移至單元'+i+'詳情"></i>'
			   +title
			   +'</div>');
	   $('#p_'+uid).off('click');
	   $('#p_'+uid).click(function(e){
		      e.preventDefault();
			  e.stopPropagation();
		      unit_print_pdf(k,$(this));
	    });
	   $('#x_'+uid).off('click');
	   $('#x_'+uid).click(function(e){
		    e.preventDefault();
			  e.stopPropagation();
	    	$.fn.fullpage.moveTo('d_'+k, 0);
	    });
	   var menu=$('<div>',{
		   class:"col-sm-9",
		   style:"min-height:28px;"
	   });
	   if (_user_info.isTeacher){
		   var cmessage=
			 '適用年級: <b>'+ all.classify[0]+'</b><br/>'+
	   		 '適用單元: <b>'+ all.classify[1]+'</b><br/>'+
	   		 '教綱編碼: <b>'+ all.classify[2]+'</b><br/>'

		   var cmessage="";
	   		$.each(all.classify,function(i,mess){
	   			cmessage+= mess+'<br/>'
	   		});
		   menu.append(
		   		 '<i id="inf_'+uid+'" class="fa fa-info fa-fw unitinfo" data-title="'+desc
		   		 +'" data-content="'+cmessage
		   		 +'"></i>');
		   menu.append('<i uid="'+uid+'" uname="'+uname+'"class="pen fa fa-pencil fa-fw" title="編修教學單元'+i+'"></i>');
		   menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="tra fa fa-trash-o fa-fw"  title="刪除教學單元'+i+'"></i>');
		   menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="u_import fa fa-cloud-download fa-fw"  title="下載教學單元'+i+'"></i>');
		   menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="u_export fa fa-cloud-upload fa-fw"  title="上傳教學單元'+i+'"></i>');
		   menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="eye fa '+(all.isBin=='0'?'fa-eye':'fa-eye-slash')+' fa-fw"  title="隱藏教學單元'+i+'"></i>');
		   menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="loc fa '+(all.isLock=='0'?'fa-unlock':'fa-lock')+' fa-fw" title="教學單元'+i+'鎖定"></i>');
		   menu.append('<span class="aib icon-stack" unitName="'
				   +all.unitName+'" unitDescription="'+all.unitDescription+'"'
				   +' title="加入教學單元在'+i+'之前">'
				   +'<i class="fa fa-plus icon-stack-3x"></i>'
				   +'<i class="fa fa-angle-double-up icon-stack-1x right_align"></i>'
				   +'</span>');
		   menu.append('<span class="aia icon-stack" unitName="'
				   +all.unitName+'" unitDescription="'+all.unitDescription+'"'
				   +' title="加入教學單元在'+i+'之後">'
				   +'<i class="fa fa-plus icon-stack-3x"></i>'
				   +'<i class="fa fa-angle-double-down icon-stack-1x right_align"></i>'
				   +'</span>');
		   //menu.append('<i unitName="'+all.unitName+'" unitDescription="'+all.unitDescription+'" class="aia fa fa-angle-double-down fa-fw" title="加入單元在'+i+'之後"></i>');
		   menu.append('<i class="fa fa-fw"></i>');
	   } //else {}
	   //menu.append('<i id="link_'+uid+'" class="fa fa-external-link fa-fw" title="外部參考"></i>');
	   //old: list of  url
	   //new: list of {title:<title>,url:<url>,type:<type>}
	   var materials=_dice_server[_choiced_mac]['teachingcycles']
		  [_save_login.base]['exercises'][uname].all.material;
	   menu.append("<b>教材</b>");
	   $.each(materials, function(i,material){
		   var link=tryParseJSON(material);
		   var url;
		   if (link){
			   menu.append('<span class="fa-stack"><i id="link_'+uid+'_'+i
					   +'" class="fa fa-circle-o fa-stack-1x fa-fw" title="'
					   +link.title+'('+link.type+')'
					   +'">'
					   +'<strong class="fa-stack-1x">'+(i+1)+'</strong>'
					   +'</i></span>');
			   url=link.url;
		   } else {
			   link=material;
			   url=link;
			   menu.append('<span class="fa-stack"><i id="link_'+uid+'_'+i
					   +'" class="fa fa-circle-o fa-stack-1x fa-fw" title="'
					   +link
					   +'">'
					   +'<strong class="fa-stack-1x">'+(i+1)+'</strong>'
					   +'</i></span>');
		   }
	   });

	   head.append(menu);

	   $.each(materials, function(i,material){
		   var link=tryParseJSON(material);
		   var url;
			 var title;
		   if (link){
			   url=link.url;
			   title=link.title;
		   } else {
			   link=material;
			   url=link;
			   title=link;
		   }
		   $('#link_'+uid+'_'+i).off('click');
		   $('#link_'+uid+'_'+i).click(function(){
			    window.open(url,title);
		   });
	   });
	   //end of menu

	   load_full(k);
		 var body=$('<div>',{
				class:'collapse row_body '+Cookies.get('_body_rb_'+i),
				id: 'rb_'+i,
				uname:uname
		 });
		 row.append(body);
	   $('#rb_'+i).bind('cssClassChanged', function(){
				 if ($('#rb_'+i).hasClass('in')){
					 Cookies.set('_body_rb_'+i, "in", { path: '/', expires: 7 });
				 } else {
					 Cookies.set('_body_rb_'+i, "", { path: '/', expires: 7 });
				 }
		 });
		 $('#unit_r_'+uname).prop('disabled', true);
     _draw_count++;
	   if ($('#rb_'+i).hasClass('in')){
		   setTimeout(function(){
			   draw_teachingunit(uname,
				 ()=>{
					 if (_exercises[uname].all.isLock=="1"){
						 //if (! _user_info.isTeacher)
							 unit_lock(uname);
					 }
					 if (_exercises[uname].all.isBin=="1"){
						 //if (! _user_info.isTeacher)
							 unit_bin(uname);
					 }
					 _draw_count--;
					 if (_draw_count<=0) {
						 set_full_click();
						 re_full();
						 if (_choiced_question){
						 		$('#pbar_'+_choiced_question.j+'_'+(_choiced_question.i+1)).click();
					   }
						 $.fn.fullpage.moveTo(_bk_r?_bk_r:_full_index, _bk_c?_bk_c:_full_slide_index);
           }
				 });
		   }, 500*Math.random());
	   } else {
		   setTimeout(function(){
				 draw_teachingunit(uname,
				 ()=>{
					 if (_exercises[uname].all.isLock=="1"){
						 //if (! _user_info.isTeacher)
							 unit_lock(uname);
					 }
					 if (_exercises[uname].all.isBin=="1"){
						 //if (! _user_info.isTeacher)
							 unit_bin(uname);
					 }
					 _draw_count--;
					 if (_draw_count<=0) {
						 set_full_click();
						 re_full();
						 if (_choiced_question){
						  	$('#pbar_'+_choiced_question.j+'_'+(_choiced_question.i+1)).click();
					   }
						 $.fn.fullpage.moveTo(_bk_r?_bk_r:_full_index, _bk_c?_bk_c:_full_slide_index);
           }
				 });
		   }, 500+500*Math.random());
	   }
     /*
	   if (all.isLock=="1"){
		   //if (! _user_info.isTeacher)
			   unit_lock(uname);
	   }
	   if (all.isBin=="1"){
		   //if (! _user_info.isTeacher)
			   unit_bin(uname);
	   }
		 */
	   //row.append('</div>');
	   //console.log((ii+1)+'/'+exlen);
	   //$("#exercises").append( '</div>');


	   if((ii+1)==exlen){
		   if (_full_all && _is_re_full &&$('#fullpage')[0]){
				$.fn.fullpage.destroy('all');
				//setting the active section as before
			    $('.section').eq(_activeSectionIndex).addClass('active');
			    //were we in a slide? Adding the active state again
			    if(_activeSlideIndex > -1){
			        $('.section.active').find('.slide').eq(_activeSlideIndex).addClass('active');
			    }
				handle_full();
			}
		   //generate();
	   }


	   //ii++;

	});

	if (_user_info.isTeacher){
		$('.pen').off('click');
		$('.pen').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			ed_unit($(this).attr('uid'),
				_dice_server[_choiced_mac]['teachingcycles'][_choiced_teachers]['exercises'][$(this).attr('uname')].all
		    );
		});
		$('.tra').off('click');
		$('.tra').click(function(e){
			  e.preventDefault();
			  e.stopPropagation();
			  var uName=$(this).attr('unitName');
			  var uDesc=$(this).attr('unitDescription');
			  swal({
				  title: '確定要刪除整個單元嗎?',
				  text: "這個動作沒有後悔藥喔!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: '是的, 刪了它!',
				  cancelButtonText: '不要, 取消吧l!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: false
				}).then(function () {
					//REMOVE IT
					swal({
						  title: '輸入密碼確認刪除',
						  input: 'password',
						  background:'red',
						  showCancelButton: true,
						  confirmButtonText: '確定',
						  showLoaderOnConfirm: true,
						  preConfirm: function (password) {
						    return new Promise(function (resolve, reject) {
						      setTimeout(function() {
						        if (!password) {
						          reject('密碼不可以空白!')
						        } else {
						          resolve();
						        }
						      }, 500)
						    })
						  },
						  allowOutsideClick: false
						}).then(function (pwd) {
							remove_unit( uName, uDesc, pwd);   //boards_functions
						}, function (dismiss) {
							  // dismiss can be 'cancel', 'overlay',
							  // 'close', and 'timer'
							  if (dismiss === 'cancel') {
							     rm_safe();
							  }
							})
					    //End of REMOVE IT
				}, function (dismiss) {
				  // dismiss can be 'cancel', 'overlay',
				  // 'close', and 'timer'
				  if (dismiss === 'cancel') {
				    rm_safe();
				  }
				})
		  });
		 $('.eye').off('click');
		 $('.eye').click(function(e){
			  e.preventDefault();
			  e.stopPropagation();
			  var uName=$(this).attr('unitName');
			  var uDesc=$(this).attr('unitDescription');
			  if ($(this).hasClass('fa-eye')){
				  $(this).toggleClass('fa-eye fa-eye-slash');
				  $(this).prop("title", "顯示單元");
				  bin_unit(uName, uDesc);
			  } else {
				  $(this).toggleClass('fa-eye-slash fa-eye');
				  $(this).prop("title", "隱藏單元");
				  unbin_unit(uName, uDesc);
			  }
		  });
		 $('.loc').off('click');
		 $('.loc').click(function(e){
			  e.preventDefault();
			  e.stopPropagation();
			  var uName=$(this).attr('unitName');
			  var uDesc=$(this).attr('unitDescription');
			  if ($(this).hasClass('fa-unlock')){
				  $(this).toggleClass('fa-unlock fa-lock');
				  $(this).prop("title", "單元解鎖");
				  lock_unit(uName, uDesc);
			  } else {
				  $(this).toggleClass('fa-lock fa-unlock');
				  $(this).prop("title", "單元鎖定");
				  unlock_unit(uName, uDesc);
			  }
		  });
		 $('.aib').off('click');
		 $('.aib').click(function(e){
			  e.preventDefault();
			  e.stopPropagation();
			  var uName=$(this).attr('unitName');
			  var uDesc=$(this).attr('unitDescription');
			  swal({
				  title: '確定要複製加入題組('+uDesc+')?',
				  text: "加入後您可以編修它!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: '是的, 加入它!',
				  cancelButtonText: '不要, 取消吧l!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: false
				}).then(function () {
					//Append
					add_unit_before(uName, uDesc);
					//End of Append
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
				  }
				})

		 });
		 $('.aia').off('click');
		 $('.aia').click(function(e){
			  e.preventDefault();
			  e.stopPropagation();
			  var uName=$(this).attr('unitName');
			  var uDesc=$(this).attr('unitDescription');
			  swal({
				  title: '確定要複製加入題組('+uDesc+')?',
				  text: "加入後您可以編修它!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: '是的, 加入它!',
				  cancelButtonText: '不要, 取消吧l!',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: false
				}).then(function () {
					//Append
					add_unit_after(uName, uDesc);
					//End of Append
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
				  }
				})
		 });
	}

	//for nav_menu
	//_nav_tcases=nav_tcases;
	$.each(_nav_tcases,function(i,nav){
		 _nav_items[_nav_items.length-1].children.push({
			 href: '#n_r_'+i,
				text: '單元'+((i*NAV_LENGTH)+1)+'至'+(((i+1)*NAV_LENGTH)),
				children:nav
		 });
	});
	$('#go_prev').off('click');
	$('#go_prev').click(function(){
		if (_flat_index>0){
			$(_flat_tcases[_flat_index-1]).click();
		} else {
			jj_notify("warn","第一題了");
		}
	});
	$('#go_next').off('click');
	$('#go_next').click(function(){
		if (_flat_index<_flat_tcases.length-1){
			$(_flat_tcases[_flat_index+1]).click();
		} else {
			jj_notify("warn","最後一題了");
		}
	});

	nav_reinit();
	if (cb) cb();
}
function gotocase(exno,j){
	var serial=_exercises[exno].serial;
	$('#go_'+serial+"_"+j).attr("title", "移至 "+serial+'-'+j);
	$('#pc_'+serial+"_"+j).attr("title", "輸出 "+serial+'-'+j);
	$('#go_'+serial+"_"+j).off('click');
	$('#go_'+serial+"_"+j).click(
			function(e){
				e.preventDefault();
				e.stopPropagation();
				$.fn.fullpage.moveTo('d_'+exno, j);
			});
	$('#pc_'+serial+"_"+j).off('click');
	$('#pc_'+serial+"_"+j).click(
			function(e){
				e.preventDefault();
				e.stopPropagation();
				case_print_pdf(exno, j,$(this));
			});
	if (_user_info.isTeacher){
		$('#rm_'+serial+"_"+j).attr("title", "删除 "+serial+'-'+j);
		$('#rm_'+serial+"_"+j).off('click');
		$('#rm_'+serial+"_"+j).click(
				function(e){
					e.preventDefault();
					e.stopPropagation();
					remove_case(j,exno);
				});
	}
}

function remove_case(i,j){
	if (! _user_info.isTeacher) {jj_notify('error','您無權刪除題目');return;};
	  var tu=_exercises[j].teachingunit[i-1];
	  swal({
		  title: '確定要刪除題目('+tu[0]+')嗎?',
		  text: "這個動作沒有後悔藥喔!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: '是的, 刪了它!',
		  cancelButtonText: '不要, 取消吧l!',
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: false
		}).then(function () {
			//REMOVE IT
			swal({
				  title: '輸入密碼確認刪除',
				  input: 'password',
				  background:'red',
				  showCancelButton: true,
				  confirmButtonText: '確定',
				  showLoaderOnConfirm: true,
				  preConfirm: function (password) {
				    return new Promise(function (resolve, reject) {
				      setTimeout(function() {
				        if (!password) {
				          reject('密碼不可以空白!')
				        } else {
				          resolve();
				        }
				      }, 500)
				    })
				  },
				  allowOutsideClick: false
				}).then(function (pwd) {
					execute('remove_case',_choiced_mac,
							{key:_choiced_teachers,unitName:j,
								case_id: ''+(i-1),cl_no:_choiced_cl_no,
								pwd:pwd,st_no:_user_info.st_no},
							function(info){
									if (info.data.data.result==-1){
										jj_notify('error','您無權操作這個動作');
									} else {
										if (_full_index>2){
										    _full_slide_index=(_full_slide_index==1)?1:_full_slide_index-1;
										}
										reload_exercises(true,true);
									}
							}
					);
				}, function (dismiss) {
					  // dismiss can be 'cancel', 'overlay',
					  // 'close', and 'timer'
					  if (dismiss === 'cancel') {
					     rm_safe();
					  }
					})
			    //End of REMOVE IT
		}, function (dismiss) {
		  // dismiss can be 'cancel', 'overlay',
		  // 'close', and 'timer'
		  if (dismiss === 'cancel') {
		    rm_safe();
		  }
		})

}
var _cache={};
var _last_item_click;
var _flat_tcases=[];
var _flat_index=0;
//var _full_slides={};
function draw_teachingunit(j,cb){
	var serial=_exercises[j].serial;
	var row=$("#rb_"+serial);
	var unit_name=row.attr('uname');
	var k=$("#unit_r_"+j).attr('col_num');
	var tcases=_exercises[k].teachingunit;
	var exlen=Object.keys(tcases).length;
	$('html,body').css('cursor','wait');
	//_full_slides[id]={};
	//_full_slides[id]['rendered']=false;
	if (exlen==0){   //沒有題目(no testcase)
		$('#ex_'+j).css('background-color', '#ff8800');
		//row.css('background-color',rgba(207, 252, 159,0.0));
		//row.css('background-color', 'red');
	}
	$.each(tcases, function(i,tcase) {
		//console.log(tcase);
		//var full_score=parseFloat(tcase[2]);
		if (_full_all && _is_re_full && $('#fullpage')[0]){
		   //_full_slides[id][i]={'tcase':tcase};
  		 $('#ss_'+serial).append(full_get_slide(i,tcase,j));   //create slides 單題
		}

		row.append(
				'<div id="tcase_'+j+'_'+(i+1)+'" style="width: 180px;" class="testcase webui col-sm-2 unit_'+j+'"'
				+' uname="'+j+'" case='+i+''
				+' data-title="'+escapeHtml(tcase[0])+'"  '
				+' data-content="'
				+escapeHtml((tcase[8]=="")?tcase[3]:tcase[8])+'">'

				+"<div class='ui-progressbar' id='pbar_"+j+"_"+(i+1)+"'>"
				+"<div class='progress-label' id='plab_"+j+"_"+(i+1)+"'>"
				+'<div class="blue-btn"><div class="first-link">'
				+(i+1)+':'
				+escapeHtml(tcase[0])
				+'</div></div>'
				+'<div id="pc_'+serial+"_"+(i+1)+'" align="right" class="fa fa-print fa-fw"></div>'
				+'<div id="go_'+serial+"_"+(i+1)+'" align="right" class="fa fa-hand-o-down fa-fw"></div>'
				+((_user_info.isTeacher)?('<div id="rm_'+serial+"_"+(i+1)+'" align="right" class="fa fa-trash-o fa-fw"></div>'):"")
				+"</div>"
				+"</div>"
				+'</div>');



		$('#pbar_'+j+'_'+(i+1)).attr('title','點擊解題');
		gotocase(j,i+1);
		//$('#plab_'+j+'_'+(i+1)).addClass('unit_'+j);
		//$('#pbar_'+j+'_'+(i+1)).addClass('unit_'+j);
		t_children[unit_name].push({
	          href: 'javascript:$.fn.fullpage.moveTo("d_'+j+'" ,'+(i+1)+');',
	          text: (i+1)+'.'+tcase[0]
	    });

		$('#pbar_'+j+'_'+(i+1)).progressbar({
		      value: 0,
		      max:parseFloat(tcase[2])
  	});

		$('#plab_'+j+'_'+(i+1)).css({
	          "background": 'rgba(100%,100%,100%,0)'
	           ,'cursor':'pointer'
	        });

		$('#pbar_'+j+'_'+(i+1)).css({
	          //"background": '#' + Math.floor( Math.random() * 16777215 ).toString( 16 ),
	          'cursor':'pointer'
	  });
		if (_users && _users.scores[j] && _users.scores[j].score[i+1]){
 				$("#pbar_"+j+"_"+(i+1)).progressbar("value",parseFloat(_users.scores[j].score[i+1][2]));
 				$("#plab_"+j+"_"+(i+1)+" .blue-btn .first-link").text(
					 "("+parseFloat(_users.scores[j].score[i+1][2]).toFixed(1)+")"+
 					 $("#plab_"+j+"_"+(i+1)+" .blue-btn .first-link").text()
 				 );
 				gotocase(j,i+1);
 		}
		//Flat_test_cases
		 _flat_tcases.push('#pbar_'+j+'_'+(i+1));
		//Click
		$('#pbar_'+j+'_'+(i+1)).off('click');
		$('#pbar_'+j+'_'+(i+1)).click(function(e){
				e.preventDefault();
				e.stopPropagation();
				_flat_index=_.indexOf(_flat_tcases, '#pbar_'+j+'_'+(i+1));
			    if (_last_item_click=='#pbar_'+j+'_'+(i+1)){
			    	if ($('#fullpage')[0]){
						    $.fn.fullpage.moveTo("full_ques", 3);
				    }
			    	return;
			    }
			    _ed_logs[$.now()]={action:'solving',title:tcase[0],u:j,q:i};
			    _last_item_click='#pbar_'+j+'_'+(i+1);
			    if (!_islogin) return;
			    $("#answers").empty();
			    $("#unit_name").text(_exercises[unit_name].serial+'-'+(i+1));
			    $("#case_name").text(tcase[0]);
			    $("#case_name").attr('i',i+1);
			    $("#case_name").attr('j',j);
			    $("#case_score").text('('+tcase[2]+')');
			    $("#case_score").attr('i',i+1);
			    $("#case_score").attr('j',j);
		    	set_ace_mode();
		    	//console.log(tcase);
		    	var txt=(tcase[8]=="")?tcase[3]:tcase[8];
		    	var temp="";
		    	//console.log("exec: "+_exec_language);
		    	if (_exec_language==='C'){
		    		temp="#include <stdio.h>\n"+
	                "int main(){\n\n"+
	                "   return 0;\n"+
	                "}\n\n";
		    	}
					var cs=$('<div/>').html(removeHTMLTag(txt.replace(/(<br>|<\/br>|<br \/>)/mgi, "\n").replace(/(<p>|<\/p>|<p \/>)/mgi, "\n"))).text();

					if (_exec_language==='Python'){
		    	  //var p=cs.split("\n"),q="";
						//$.each(p,function(k,v){q+="#"+v+"\n";});
					  //	_ace.setValue(temp+q);
						_ace.setValue(temp+"'''\n"+cs+"\n'''\n");
          } else {
						_ace.setValue(temp+'/*'+cs+'*/');
					}

		    	//console.log("____11_____");
		    	//$("#ans_filename").text('web.c');

		    	/*
			    if (_cache['#pbar_'+j+'_'+(i+1)]){
			    	handle_answers( _cache['#pbar_'+j+'_'+(i+1)] , unit_name);
			    	if ($('#fullpage')[0]){
						$.fn.fullpage.moveTo('full_ques',3);
				    }
			    	return;
			    }
			    */
			    _choiced_question={
				    	para:{id:$("#id").val(),
								cl_no:_choiced_cl_no,
								base:_choiced_teachers,st_no:_user_info.st_no,
								item:(i+1)+'',exid:j+''},
        				tcase:tcase,
        				j:j,i:i,
        				unit_name:unit_name,
        				filename:$("#ans_filename").text()};

			    if (_user_info.isTeacher){
			    	$('#save_ref').show();
			    	handle_ref_ans(j,i,unit_name);
			    } else {
			    	$('#save_ref').hide();
						execute('get_user_code',_choiced_mac,
								_choiced_question.para,
								function(info){
									 //_cache['#pbar_'+j+'_'+(i+1)]=info;
									$("#ans_title").html('作答');
									 handle_answers(info,unit_name);

								   if ($('#fullpage')[0] && ! _bk_r){
											 $.fn.fullpage.moveTo('full_ques', 3);
									 } else {
										 if (_re_loading){
					 						if (sessionStorage.getItem('_editorSession')) jsonToSession(_ace, JSON.parse(sessionStorage.getItem('_editorSession')));
					 						_re_loading=false;
					 					 }
									 }

						});
			    }

		});
	});
  $("#unit_r_"+j).prop('disabled', false);

	if (_user_info.isTeacher){
		$('.row_body,#exercises').sortable( "enable" );
	}	else {
		$('.row_body,#exercises').sortable( "disable" );
	}
	$('html,body').css('cursor','default');
	$('.webui').webuiPopover({placement:'auto',
		trigger:'hover',
		width:'600px',
		height:'300px',
		animation:'pop',
		delay: {
			show: 2000,
	        hide: 300
	    },
		closeable:true});
	if (cb) cb();
}

function set_full_click(){
		$('.unitinfo').webuiPopover({placement:'auto',
			trigger:'hover',
			width:'auto',
			animation:'pop',
			delay: {
				show: null,
		        hide: 300
		    },
			closeable:true});
		$('.open_d3x').off('click');
		$('.open_d3x').click(
				function(e){
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					let serial=$(this).attr('serial');
					if (_is_total_scores){
		        echarts_stack(serial);   //board_d3
		      } else {
		        get_class_score(true,function(){
		          echarts_stack(serial);
		        });
		      }
		});
		$('.gomenu').off('click');
		$('.gomenu').click(
				function(e){
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					$.fn.fullpage.moveTo("full_ques", 2);
		});
		$('.goans').off('click');
		$('.goans').click(
				function(e){
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					$('#plab_'+$(this).attr('j')+'_'+$(this).attr('i')).click();
		});

		$('.getcodes').off('click');
		$('.getcodes').click(
				function(e){
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					let code=new Codes($(this).attr('j'),$(this).attr('i'),
						(codes)=>{
							code.showWindow();
						}
					);
		});
		$('.q_append').off('click');
		$('.q_append').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權加入題目');return;};
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					_bk_r=_full_index;_bk_c= _full_slide_index;
					 swal({
						  title: '確定要複製題目('+tu[0]+'), 並加入題組嗎?',
						  text: "加入後您可以編修它!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: '是的, 加入它!',
						  cancelButtonText: '不要, 取消吧l!',
						  confirmButtonClass: 'btn btn-success',
						  cancelButtonClass: 'btn btn-danger',
						  buttonsStyling: false
						}).then(function () {
							//Append
							execute('append_case',_choiced_mac,
									{key:_choiced_teachers,unitName:j,
										case_id: ''+(i-1),cl_no:_choiced_cl_no,
										pwd:_pwd,st_no:_user_info.st_no},
									function(info){
											if (info.data.data.result==-1){
												jj_notify('error','您無權操作這個動作');
											} else {
												reload_exercises(true,true);
											}
									}
							);
							//End of Append
						}, function (dismiss) {
						  if (dismiss === 'cancel') {
						  }
						})
		});
		$('.edsco').off('click');
		$('.edsco').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權修改配分');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					var osco=tu[2];  //old score
					swal({
						  title: '修改相對配分',
						  text: '配分為相對值介於0-100',
						  html: '<input value="'+osco+'" id="swal-sco" class="swal2-input">',
						  background:'#9ec0f7',
						  showCancelButton: true,
						  confirmButtonText: '確定',
						  showLoaderOnConfirm: true,
						  preConfirm: function (password) {
						    return new Promise(function (resolve, reject) {
						      setTimeout(function() {
						        if (! $('#swal-sco').val()) {
						          reject('分數不可以空白!');
						        } else if (! ($('#swal-sco').val()>=0 && $('#swal-sco').val()<=100) ) {
						          reject('分數必須介於0~100!')
						        } else {
						          resolve($('#swal-sco').val());
						        }
						      }, 500)
						    })
						  },
						  allowOutsideClick: false
						}).then(function (sco) {
							execute('save_score',_choiced_mac,
									{key:_choiced_teachers,unitName:j,
										case_id: ''+(i-1),cl_no:_choiced_cl_no,
										score:sco,pwd:_pwd,st_no:_user_info.st_no},
									function(info){
											if (info.data.data.result==-1){
												jj_notify('error','您無權操作這個動作');
											} else {
												reload_exercises(true,true);
											}
									}
							);
						}, function (dismiss) {
							  if (dismiss === 'cancel') {
							  }
						})
		});
		$('.edtitle').off('click');
		$('.edtitle').click(
				function(e){
					_bk_r=_full_index;_bk_c= _full_slide_index;
					if (! _user_info.isTeacher) {jj_notify('error','您無權修改名稱');return;};
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					var osco=tu[0];  //old title
					swal({
						  title: '修改題目名稱',
						  html: '<input value="'+osco+'" id="swal-sco" class="swal2-input">',
						  background:'#9ec0f7',
						  showCancelButton: true,
						  confirmButtonText: '確定',
						  showLoaderOnConfirm: true,
						  preConfirm: function (password) {
						    return new Promise(function (resolve, reject) {
						      setTimeout(function() {
						        if (! $('#swal-sco').val()) {
						          reject('題目名稱不可以空白!');
						        } else {
						          resolve($('#swal-sco').val());
						        }
						      }, 500)
						    })
						  },
						  allowOutsideClick: false
						}).then(function (sco) {
							execute('save_title',_choiced_mac,
									{key:_choiced_teachers,unitName:j,
										case_id: ''+(i-1),cl_no:_choiced_cl_no,pwd:_pwd,st_no:_user_info.st_no,
										title:sco},
									function(info){
										if (info.data.data.result==-1){
											jj_notify('error','您無權操作這個動作');
										} else {
											reload_exercises(true,true);
										}
									}
							);
						}, function (dismiss) {
							  if (dismiss === 'cancel') {
							  }
						})
		});
		$('.q_import').off('click');
		$('.q_import').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權下載題目');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					var id=_choiced_cl_no+'_'+j+'_'+tu[0];
					jj_notify('info','開始下載'+id);
					execute('import_case',_choiced_mac,
							{key:_choiced_teachers,unitName:j,
								case_id: ''+(i-1),cl_no:_choiced_cl_no,
								pwd:_pwd,st_no:_user_info.st_no},
							function(info){
									if (info.data.data.result==-1) jj_notify('error','您無權操作這個動作');
									else {
										//console.log(info);
										var mm=info.data.data.mm;
										var json=info.data.data.json;
										var inputOptions = new Promise(function (resolve) {
											    resolve({
											      'mm': 'Mindmap',
											      'json': 'Json'
											    })
											})

										swal({
											  title: '選擇資料類型',
											  text: 'Mindmap: freemind mm檔\n Json: json 檔',
											  input: 'radio',
											  inputOptions: inputOptions,
											  inputValidator: function (result) {
											    return new Promise(function (resolve, reject) {

											    	resolve();
											    })
											  }
											}).then(function (result) {
											  if (result=='mm'){
												  savefile(id+".mm",mm);
											  } else if (result=='json'){
												  savefile(id+".json",json);
											  } else {
												  savefile(id+".mm",mm);
												  savefile(id+".json",json);
											  }
											})
									}
							}
					);

		});
		$('.q_export').off('click');
		$('.q_export').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權上傳題目');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					var id=_choiced_cl_no+'_'+j+'_'+(i-1);
					swal({
						  title: '選擇mm檔',
						  text: '請選擇一個題目的心智圖檔',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: '選定上傳',
						  cancelButtonText: '放棄',
						  confirmButtonClass: 'btn btn-success',
						  cancelButtonClass: 'btn btn-danger',
						  input: 'file',
						  inputAttributes: {
							    accept: 'text/x-troff-mm'
						  }
						}).then(function (file) {
							//console.log(file);
							var reader = new FileReader();
							reader.onload = function (e) {
								  var xml=e.target.result;
								  if (isXML(xml)){
									  jj_notify('info','開始上傳');
									  execute('export_case',_choiced_mac,
											  {key:_choiced_teachers,unitName:j,
												case_id: ''+(i-1),cl_no:_choiced_cl_no,
												xml:xml,filename:id,
												pwd:_pwd,st_no:_user_info.st_no},
												function(info){
														if (info.data.data.result==-1){
															jj_notify('error','您無權操作這個動作');
														} else if (info.data.data.result==0){
															jj_notify('success','上傳完成, 更新中..');
															reload_exercises(true,true);
														} else if (info.data.data.result==-3){
															jj_notify('error','上傳失敗, 格式有誤');
															swal({
																  title: '上傳失敗, 格式有誤',
																  input: 'textarea',
																  inputValue: info.data.data.message
															})
														} else {
															jj_notify('error','上傳失敗: '+info.data.data.message);
														}
													}
									  );
								  } else {
									  swal({
												  title: '格式有誤',
												  type: 'error',
												  text: file.name+' 不是合格的題目心智圖檔.',
												  timer: 10000
									  			}
											);
								  }
							 }
							 reader.readAsText(file)
						})
		});
		$('.u_import').off('click');
		$('.u_import').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權下載教學單元');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('unitName');
					var id=_choiced_cl_no+'_'+j;
					execute('import_unit',_choiced_mac,
							{key:_choiced_teachers,unitName:j,
								cl_no:_choiced_cl_no,
								pwd:_pwd,st_no:_user_info.st_no},
							function(info){
									if (info.data.data.result==-1) jj_notify('error','您無權操作這個動作');
									else {
										//console.log(info);
										var mm=info.data.data.mm;
										var json=info.data.data.json;
										var inputOptions = new Promise(function (resolve) {
											    resolve({
											      'mm': 'Mindmap',
											      'json': 'Json'
											    })
											})

										swal({
											  title: '選擇資料類型',
											  text: 'Mindmap: freemind mm檔\n Json: json 檔',
											  input: 'radio',
											  inputOptions: inputOptions,
											  inputValidator: function (result) {
											    return new Promise(function (resolve, reject) {
											    	resolve();
											    })
											  }
											}).then(function (result) {
											  if (result=='mm'){
												  savefile(id+".mm",mm);
											  } else if (result=='json'){
												  savefile(id+".json",json);
											  } else {
												  savefile(id+".mm",mm);
												  savefile(id+".json",json);
											  }
											})
									}
							}
					);

		});
		$('.u_export').off('click');
		$('.u_export').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權上傳教學單元');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('unitName');
					var id=_choiced_cl_no+'_'+j;
					var radio='<div id="type_choice" class="btn-group" data-toggle="buttons">'+
							        '   <label class="btn btn-default active">'+
							        '        <input checked type="radio" class="toggle" value="1"/> 覆蓋本題'+
							        '    </label>'+
							        '    <label class="btn btn-default">'+
							        '        <input type="radio" class="toggle" value="2" /> 加至末端'+
							        '    </label>'+
							        '</div>';
					swal({
						  title: '選擇一個教學單元的心智圖mm檔',
						  //text: '請選擇一個教學單元的心智圖檔',
						  html: radio,
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: '選定上傳',
						  cancelButtonText: '放棄',
						  confirmButtonClass: 'btn btn-success',
						  cancelButtonClass: 'btn btn-danger',
						  input: 'file',
						  inputAttributes: {
							    accept: 'text/x-troff-mm'
						  }
						}).then(function (file) {
							var type=$('#type_choice label.active input').val();
							var reader = new FileReader();
							reader.onload = function (e) {
								  var xml=e.target.result;
								  if (isXML(xml)){
									  jj_notify('info','開始上傳教學單元'+(type=="1"?"覆蓋本題":"加至末端"));
									  execute('export_unit',_choiced_mac,
											  {key:_choiced_teachers,unitName:j,
												cl_no:_choiced_cl_no,
												xml:xml,filename:id,type:type,
												pwd:_pwd,st_no:_user_info.st_no},
												function(info){
														if (info.data.data.result==-1){
															jj_notify('error','您無權操作這個動作');
														} else if (info.data.data.result==0){
															jj_notify('success','上傳完成, 更新中..');
															reload_exercises(true,true);
														} else if (info.data.data.result==-3){
															jj_notify('error','上傳失敗, 格式有誤');
															swal({
																  title: '上傳失敗, 格式有誤',
																  input: 'textarea',
																  inputValue: info.data.data.message
															})
														} else {
															jj_notify('error','上傳失敗: '+info.data.data.message);
														}
													}
									  );

								  } else {
									  swal({
												  title: '格式有誤',
												  type: 'error',
												  text: file.name+' 不是合格的教學單元心智圖檔.',
												  timer: 10000
									  			}
											);
								  }

							 }

							 reader.readAsText(file)
						})
		});
		$('.q_edit').off('click');
		$('.q_edit').click(
				function(e){
					if (! _user_info.isTeacher) {jj_notify('error','您無權修改內容');return;};
					_bk_r=_full_index;_bk_c= _full_slide_index;
					e.preventDefault();
					e.stopPropagation();
					var j=$(this).attr('j');
					var i=$(this).attr('i');
					var tu=_exercises[j].teachingunit[i-1];
					var id='#e_ls_'+_exercises[j].serial+'_'+(i-1);
					if (! _tinymce_inited[id]){
						assign_editable(id)
					} else {
						tinymce.remove(id);
						_tinymce_inited[id]=false;
					}
		});

		$('.q_remove').off('click');
		$('.q_remove').click(function(e){
			 e.preventDefault();
			 e.stopPropagation();
			 remove_case($(this).attr('i'),$(this).attr('j'));
		});

}

function re_full(){
	$.fn.fullpage.destroy('all');
	init_full_setting();
	//$.each(_exercises, function(k, v) {
	$.each(_exercise_keys,function(ii,k){
		 var i=ii+1;
		_full_setting.anchors.push('d_'+k);
		_full_setting.navigationTooltips.push(_exercises[k].serial+'-'+_exercises[k].name);
	});
	handle_full();
}
