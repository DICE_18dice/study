var D_COMM = function(id,url,cmd){
    this.c_id = id;
    this.ms_url = url;
    this.is_connected=false;
    this.is_plugin=false;
    this.mac=sessionStorage.getItem('_mac'+this.id);
    this.users={};
    this.userinfo=null;
    this.is_monitor_on=false;
    this.monitor_enevts=[];
    this.monitors={};
    this.commands=cmd;
}



D_COMM.prototype.connect = function(cb) {
  this.ds_socket = io.connect(this.ms_url+'/MessageServer?info='
                  +JBase64.encode((this.mac)?'{mac:"'+this.mac+'"}':'{}'))
  .on('connect', () => {
      this.is_connected=true;
  		this.ds_socket.emit("plugin"
  	 			,(this.mac)?{mac:this.mac}:{}
  	 			,(data)=>{
  			 		if (data.result==0){
  			 			this.ip=data.data.ip;
              this.mac=data.data.mac;
              this.is_plugin=true;
              sessionStorage.setItem('_mac'+this.id,this.mac);
  			 		}
            if (cb) cb(this.is_plugin);
  	 	});
     })
  .on('misc#welcome', (data) => {
     this.is_connected=true;
   })
  .on('error', (err) => {
     this.is_connected=false;
   })
  .on('disconnect', (err) => {
    this.is_connected=false;
   })
  .on('reconnect', (times) => {
    this.is_connected=true;
   })
  .on('reconnecting', (times) => {
    this.is_connected=false;
  })
  .on('reconnect_failed', () => {
    this.is_connected=false;
  })
  .on('reconnect_error', (err) => {
    this.is_connected=false;
  })
  .on('ports_changed', (result) => {
  })
  .on('joined', (result) => {
    console.log("joined");
    console.log(result);
  })
  .on('left', (result) => {
    console.log("left");
    console.log(result);
  })
  .on('watch_it', (result) => {
    if (result.event==='disconnect' || result.event==='left'){
      $.each(this.users,(r,u)=>{
        if (u[result.mac]) delete u[result.mac];
      });
      $.each(this.monitors,(r,u)=>{
        if (u[result.mac]) delete u[result.mac];
      });
    }
  })
  .on('message', (result) =>{
    handle_command(result);
  })
  .on('broadcast_room',(result) => {
    handle_command(result);
  });


  var handle_command = (data)=>{
    //console.log(this.commands[data.data.data.comm]);
    if (data.data.data && this.commands[data.data.data.comm]) 
       eval(this.commands[data.data.data.comm]);
    else { 
       console.log(data);
    }  
  }
}
D_COMM.prototype.get_rooms = function() {
    return Object.keys(this.users);
}
D_COMM.prototype.join = function(room,user,cb) {
    this.userinfo=user;
    this.ds_socket.emit("join"
    ,room,
  	(r)=>{
      if (r.result===0){
        this.get_users(room,()=>{
          this.broadcast(room, {comm:'user_login',data:user});
          }
        );

      }
  		if (cb) cb(r);
  	});
}

D_COMM.prototype.monior_on = function(mevents,room,cb,extra,mac) {
  if (!cb) cb = ()=>{};
  if (!extra) extra = ()=>{};
  if (! mac){
    this.broadcast(room,{comm:'monitor_on',data:mevents
        ,cbstr:cb.toString(),extra:extra.toString()});
  } else {
    this.send(mac,{comm:'monitor_on',data:mevents
        ,cbstr:cb.toString(),extra:extra.toString(),room:room});
  }
}

D_COMM.prototype.monior_off = function(room,mevents) {
  this.broadcast(room,{comm:'monitor_off'});
  /*
  $(window).off(mevents);
  */
}
D_COMM.prototype.broadcast_all = function(data,cb) {
  this.ds_socket.emit("broadcast_all",
			{data:data},
			(r)=>{
        if (cb) cb(r);
      }
	);
}
D_COMM.prototype.broadcast = function(room,data,cb) {
  this.ds_socket.emit("broadcast_room",
			room,{data:data},
			(r)=>{
        if (cb) cb(r);
      }
	);
}
D_COMM.prototype.send = function(xmac,data,callback,ps_room){
  if (ps_room){
	  this.ds_socket.emit('send_to',
				xmac,
				{data:data},
        (r)=>{
          if (cb) cb(r);
        },ps_room
		);
	} else {
		this.ds_socket.emit('send_to',
				xmac,
				{data:data},
        (r)=>{
          if (cb) cb(r);
        }
		);
	}
}
D_COMM.prototype.get_users = function(room,cb) {
  this.ds_socket.emit('broadcast_room',
			room,{data:{comm:'get_users'}},
			(r)=>{
        if (cb) cb(r);
      }
	);
}
