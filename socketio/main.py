import base64
import asyncio
import socketio
import aiohttp

loop = asyncio.get_event_loop()
sio = socketio.AsyncClient()


mac=base64.b64encode(bytes('{}', 'utf-8'))

# standard Python
sio = socketio.AsyncClient()

@sio.on('connect')
async def on_connect():
    print('Connected!')


@sio.on('disconnect')
async def on_disconnect():
    print('Disconnected!')

@sio.on('message')
async def on_message(data):
    print('I received a message!')

@sio.on('misc#welcome')
async def on_misc_welcome(data):
    print('I received a welcome message! ')


async def start_server():
    await sio.connect("http://127.0.0.1:6070/",namespaces=["/MessageServer?info="+mac.decode('UTF-8', 'ignore')])
    await sio.wait()

if __name__ == '__main__':
    loop.run_until_complete(start_server())
