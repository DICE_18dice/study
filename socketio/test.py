from socketIO_client_nexus import SocketIO, BaseNamespace
from base64 import b64encode

mac=b64encode(bytes('{}', 'utf-8'))

class MSNamespace(BaseNamespace):

    def on_connect(self):
        print('[Connected]') 

    def on_reconnect(self):
        print('[Reconnected]')

    def on_disconnect(self):
        print('[Disconnected]')

    def on_welcome(*args):
        print('on_welcome_response', args)



socketIO = SocketIO('140.123.95.186', 6070, MSNamespace,params={'info': mac})
mss = socketIO.define(MSNamespace, '/MessageServer')

# Listen
mss.on('connect', MSNamespace.on_connect)
mss.on('disconnect', MSNamespace.on_disconnect)
mss.on('reconnect', MSNamespace.on_reconnect)
mss.on('misc#welcome', MSNamespace.on_welcome)

#mss.emit('plugin')

socketIO.wait()


